# This file is part of open-fpsz.
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
extends Node3D

@onready var fire = $Fire
@onready var explosion_area = $Area3D
var explosion_effect_pending : bool = false

const impulse_force = 1000;

func explode():
	explosion_effect_pending = true
	fire.emitting = true
	
func _physics_process(_delta):
	if explosion_effect_pending:
		var bodies = explosion_area.get_overlapping_bodies()
		
		for body in bodies:
			if body is RigidBody3D:
				var direction = (body.global_position - global_position).normalized()
				body.apply_central_impulse(direction * impulse_force)
		
		explosion_effect_pending = false
		await get_tree().create_timer(1.0).timeout
		queue_free()
