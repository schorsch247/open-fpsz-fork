# This file is part of open-fpsz.
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
extends Node3D

const speed : float = 68.0
var velocity : Vector3 = Vector3.ZERO

# @FIXME: with an imaginary box around the map
const lifespan : float = 5.0 # in seconds

@onready var shape_cast = $ShapeCast3D
@onready var game = get_tree().get_current_scene()

const EXPLOSION = preload("res://weapons/space_gun/projectile_explosion.tscn")

func _ready():
	var lifespan_timer = Timer.new()
	lifespan_timer.set_wait_time(lifespan)
	lifespan_timer.set_one_shot(true)
	lifespan_timer.set_autostart(true)
	lifespan_timer.timeout.connect(self_destruct)
	add_child(lifespan_timer)

func self_destruct():
	explode(position)

func explode(spawn_location):
	var spawned_explosion = EXPLOSION.instantiate()
	spawned_explosion.position = spawn_location
	game.add_child(spawned_explosion)
	spawned_explosion.explode()
	queue_free()

func _physics_process(delta):
	global_position += velocity * delta
	shape_cast.target_position = to_local(global_position - velocity) * delta
	if shape_cast.is_colliding():
		var contact_point = shape_cast.collision_result[0].point
		explode(contact_point)
