# This file is part of open-fpsz.
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
extends Node3D
class_name SpaceGun

signal shoot(projectile, velocity, initial_location, initial_rotation)

const PROJECTILE = preload("res://weapons/space_gun/projectile.tscn")

@onready var nozzle = $Nozzle
@onready var inventory = get_parent()

const ammo_max : int = 20
var ammo : int = ammo_max

var inheritance : float = 100.0  # percent

func print_node_properties(node):
	var properties = node.get_property_list()
	for prop in properties:
		prints(prop.name, node.get(prop.name))

func _ready():
	shoot.connect(_on_shoot)

func _input(_event):
	if Input.is_action_just_pressed("fire_primary"):
		var projectile = PROJECTILE.instantiate()
		shoot.emit(projectile, nozzle, inventory.owner)

func _on_shoot(projectile, origin, player):
	projectile.position = origin.global_position
	projectile.rotation = origin.global_rotation
	projectile.velocity = origin.global_basis.z.normalized() * projectile.speed
	var inheritance_factor = clamp(inheritance, 0., 100.) / 100.
	projectile.velocity += (player.linear_velocity * inheritance_factor)
	inventory.owner.add_sibling(projectile)
	var collider = projectile.shape_cast
	collider.add_exception(player)
